﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Image = UnityEngine.UI.Image;

public class Player2Controller : MonoBehaviour {

	float speed = 0.6f;
	private Rigidbody rb;

	public UImanager2 ui;

	public AudioManager audioManager;

	void Start () {
		audioManager.backroundSound.Play ();
		rb = GetComponent<Rigidbody> ();
		rb.AddForce (new Vector3 (0f, 0f, 10f) * speed);
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.RightArrow)) {
			transform.position += Vector3.right * speed;//* Time.deltaTime;
		}
		if (Input.GetKey (KeyCode.LeftArrow)) {
			transform.position += Vector3.left * speed;//* Time.deltaTime;
		}
		if (Input.GetKey (KeyCode.UpArrow)) {
			transform.position += Vector3.forward * speed;// * Time.deltaTime;
		}
		if (Input.GetKey (KeyCode.DownArrow)) {
			transform.position += Vector3.back * speed;// * Time.deltaTime;
		}
			
		/*if (!temp) {
			left1.gameObject.SetActive (true);
			left2.gameObject.SetActive (false);

			right1.gameObject.SetActive (true);
			right2.gameObject.SetActive (false);
			temp = true;
		} else {
			left1.gameObject.SetActive (false);
			left2.gameObject.SetActive (true);

			right1.gameObject.SetActive (false);
			right2.gameObject.SetActive (true);
			temp = false;
		}*/
	}

	void OnCollisionEnter(Collision collision) {
		if (collision.gameObject.tag == "Enemy") {
			audioManager.dustbinHitSound.Play ();
			ui.GameOverActivated ();
			this.gameObject.SetActive (false);

		} else if (collision.gameObject.tag == "PalrkingCars") {
			audioManager.parkingCarHitSound.Play ();
			ui.GameOverActivated ();
			this.gameObject.SetActive (false);
		} else if (collision.gameObject.tag == "HighwayCars") {			
			audioManager.highwayCarHitSound.Play ();
			ui.GameOverActivated ();
			this.gameObject.SetActive (false);
		} else if (collision.gameObject.tag == "Balls") {			
			audioManager.ballHitSound.Play ();
			ui.GameOverActivated ();
			this.gameObject.SetActive (false);
		} else if (collision.gameObject.tag == "Dustbins") {			
			audioManager.dustbinHitSound.Play ();
			ui.GameOverActivated ();
			this.gameObject.SetActive (false);
		} else if (collision.gameObject.tag == "Manhole") {			
			audioManager.manholeHitSound.Play ();
			ui.GameOverActivated ();
			this.gameObject.SetActive (false);
		} 

	}
}
