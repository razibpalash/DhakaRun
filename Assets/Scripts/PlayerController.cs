﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Image = UnityEngine.UI.Image;
public class PlayerController : MonoBehaviour {
	public float speed = 5f;
	public int playerHealth;
	int damage = 1;
	Image healthBar;
	private Rigidbody rb;
	public UIManager ui;
	public AudioManager audioManager;
	void Start () {
		audioManager.backroundSound.Play ();
		rb = GetComponent<Rigidbody> ();
		healthBar = GameObject.Find ("Camera").transform.FindChild ("Canvas").FindChild ("HealthBar").GetComponent<Image> ();
		rb.AddForce (new Vector3 (0f, 0f, 10f) * speed);
	}
	void FixedUpdate () {
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");
		Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);
		rb.AddForce (movement * speed);
		if (playerHealth <= 0) {
			ui.GameOverActivated ();
			this.gameObject.SetActive (false);
		}
	}
	void OnCollisionEnter(Collision collision) {
		if (collision.gameObject.tag == "Enemy") {
			audioManager.dustbinHitSound.Play ();
			healthBar.fillAmount -= 0.1f;
			playerHealth -= damage;
		} else if (collision.gameObject.tag == "PalrkingCars") {
			healthBar.fillAmount -= 0.1f;
			playerHealth -= damage;
			audioManager.parkingCarHitSound.Play ();
		} else if (collision.gameObject.tag == "HighwayCars") {
			healthBar.fillAmount -= 0.1f;
			playerHealth -= damage;
			audioManager.highwayCarHitSound.Play ();
		} else if (collision.gameObject.tag == "Balls") {
			healthBar.fillAmount -= 0.1f;
			playerHealth -= damage;
			audioManager.ballHitSound.Play ();
		} else if (collision.gameObject.tag == "Dustbins") {
			healthBar.fillAmount -= 0.1f;
			playerHealth -= damage;
			audioManager.dustbinHitSound.Play ();
		} else if (collision.gameObject.tag == "Manhole") {
			healthBar.fillAmount -= 0.1f;
			playerHealth -= damage;
			audioManager.manholeHitSound.Play ();
		} else if (collision.gameObject.tag == "Dirt") {
			healthBar.fillAmount -= 0.1f;
			playerHealth -= damage;
			audioManager.dirtHitSound.Play ();
		}
	}
}
