﻿using UnityEngine;
using System.Collections;

public class MoveLeftNRight : MonoBehaviour {

	public float left;
	public float right;
	Vector3 movement = Vector3.left * 0.1f;


	void Start () {
		
	}

	// Update is called once per frame
	void Update () {
		if (transform.position.x > right) {
			movement = Vector3.left * 0.1f;
		} else if (transform.position.x < left) {
			movement = Vector3.right * 0.1f;
		} 
		transform.Translate (movement);
	}
}
