﻿using UnityEngine;
using System.Collections;

public class MoveForwardNBackward : MonoBehaviour {
	
	public float forwardPos;
	public float backwardPos;
	Vector3 movement = Vector3.back * 0.1f;
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		if (transform.position.z < backwardPos) {
			movement = Vector3.forward * 0.1f;
		} else if (transform.position.z > forwardPos) {
			movement = Vector3.back * 0.1f;
		}
		transform.Translate (movement);
	}
}
