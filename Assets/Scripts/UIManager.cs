﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

	public Text scoreText;
	public Text highScoreText;
	bool gameOver;
	int score;


	public Button[] buttons;
	public Button exitButton;
	public Button pauseButton;
	public Sprite playImg;
	public Sprite pauseImg;

	// Use this for initialization
	void Start () {
		score = 0;


		gameOver = false;
		InvokeRepeating ("ScoreUpdate", 1.0f, 0.5f);
	}
	
	// Update is called once per frame
	void Update () {
		
		scoreText.text = "Score : " + score;
	}


	void ScoreUpdate(){
		if (!gameOver) {
			score += 3;
		}
	}

	public void GameOverActivated(){
		gameOver = true;
		foreach (Button button in buttons) {
			button.gameObject.SetActive (true);
		}
	}

	public void Play(){
		Application.LoadLevel ("Level 1");
	}

	public void Pause(){
		if (Time.timeScale == 1) {
			Time.timeScale = 0;
			exitButton.gameObject.SetActive (true);
			pauseButton.image.sprite = playImg;
		} else if(Time.timeScale == 0) {
			Time.timeScale = 1;
			exitButton.gameObject.SetActive (false);
			pauseButton.image.sprite = pauseImg;

		}
	}

	public void RePlay(){
		Application.LoadLevel (Application.loadedLevel);
	}
	public void Menu(){
		
	}
	public void Exit(){
		Application.Quit ();
	}
}
