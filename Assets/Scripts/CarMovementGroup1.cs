﻿using UnityEngine;
using System.Collections;

public class CarMovementGroup1 : MonoBehaviour {
	
	public float minPos;
	public float maxPos;
	Vector3 movement = Vector3.right * 0.5f;
	void Start () {
		
	}


	void Update () {
		if (transform.position.x > maxPos) {
			if (transform.position.x >= maxPos || transform.position.x <= maxPos) {
				transform.Rotate (0, 180, 0);
			}
		} else if (transform.position.x < minPos) {
			if (transform.position.x >= minPos || transform.position.x <= minPos) {
				transform.Rotate (0, 180, 0);
			}
		}

		transform.Translate (movement);
	}
}
