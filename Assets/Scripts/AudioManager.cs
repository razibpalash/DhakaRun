﻿using UnityEngine;
using System.Collections;

public class AudioManager : MonoBehaviour {

	public AudioSource backroundSound;
	public AudioSource hiteSound;
	public AudioSource ballHitSound;
	public AudioSource parkingCarHitSound;
	public AudioSource highwayCarHitSound;
	public AudioSource dustbinHitSound;
	public AudioSource manholeHitSound;
	public AudioSource dirtHitSound;

	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
