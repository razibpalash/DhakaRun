﻿using UnityEngine;
using System.Collections;

public class AutoMoveBackward : MonoBehaviour {

	float speed = 5f;
	void Start () {

	}

	void FixedUpdate () {
		transform.Translate (Vector3.back * speed * Time.deltaTime, Space.Self);
	}
}
